const HtmlWebPackPlugin = require('html-webpack-plugin');

const htmlPlugin = new HtmlWebPackPlugin({
    template: './src/index.html',
    filename: './index.html',
});

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          'loader': 'babel-loader'
        }
      }
    ]
  },
  plugins: [htmlPlugin]
}
/**
 * The configuration in this file tells webpack to use babel-loader for every file that has the .js extension and excludes .js files in the node_modules directory for the Babel compiler. The actual settings for babel-loader are placed in a separate file, called .babelrc.
 */